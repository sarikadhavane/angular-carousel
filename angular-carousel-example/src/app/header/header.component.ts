import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isSearch:boolean=false;
  constructor() { }

  ngOnInit() {
  }
  toggleSearch(){
    console.log('i')
    this.isSearch =!this.isSearch;
  }
}
