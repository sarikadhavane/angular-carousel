import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SlideService {

  constructor() { }
  getSlidesData(): object{
    return  [
      { url: 'assets/images/slide.png', text: 'Mobile internet'},
      { url: 'assets/images/slide.png', text: 'Home internet'},
      { url: 'assets/images/slide.png', text: 'Get a device'},
      { url: 'assets/images/slide.png', text: 'Add a phone-line'},
      { url: 'assets/images/slide.png', text: 'Upgrade'} 
    ];
  }
}
